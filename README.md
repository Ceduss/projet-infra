# Projet Infra

Bienvenue sur le repository de notre projet infra. 

Nous avons décidé de créer une solution de déploiement automatique d'un VPN.

---

- [Pour accéder à la doc d'installation du VPN](./Documents/Documention.md)
- [Script pour installer le VPN sur le client](./Documents/installationClient.sh)
- [Script pour installer le VPN sur le serveur](./Documents/installationServeur.sh)