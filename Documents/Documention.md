CONFIGURATION DE WIREGUARD EN PROPRE

Liste Ip:
Serveur: 192,168,202,20/24
Client: 192,168,202,21/24

Installation de wireguard

Sur les deux machines:

On ajoute le repo de wireguard a la machine:

`echo 'deb http://ftp.debian.org/debian bulleye-backports main' | sudo tee /etc/apt/sources.list.d/bulleye-backports.list`

On update la machine puis on installe wireguard ainsi que les package dont on va avoir besoin:

`sudo apt update`

`sudo apt install wireguard`

`sudo apt install iptables`

`sudo apt install ufw`


Configuration Wireguard sur le serveur

Sur le serveur:

On execute la commande qui va générer une clé publique et une clé privée en ajoutant un fichier `publickey` et un fichier `privatekey` dans le dossier /etc/wireguard.

On récupère la clé public et la clé privée:

`sudo cat /etc/wireguard/privatekey`

`sudo cat /etc/wireguard/publickey`

Output:

Pv : oJiTk5ibwxcUiIHymslUwdsmLugKqMhIMbNZql6y40o=

Pb : Lv+jzTgyGwx74maDX83oGOOKg0+vKsvFwGptPI5aLH8=

On regarde le nom de notre réseau internet public avec la commande suivante (important pour plus tard):

`ip -o -4 route show to default | awk '{print $5}'`

On crée le fichier de conf de wireguard avec la commande:

`sudo nano /etc/wireguard/wg0.conf`

on y ajoute le texte suivant:

```bash
[Interface]
Address = 10.0.0.1/24
SaveConfig = true
ListenPort = 51820
PrivateKey = LA_CLE_PRIVEE_DU_SERVEUR
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o LE_NOM_VOTRE_CARTE_RESEAU_ -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o ens3 -j MASQUERADE
```

On permet au utilisateur de lire le dossier wg0 et le dossier contenant la clé privé:

`sudo chmod 600 /etc/wireguard/{privatekey,wg0.conf}`

on lance la configuration rapide via le dossier wg0:

`sudo wg-quick up wg0`

Le résultat devrait ressembler ça:

```bash
[#] ip link add wg0 type wireguard
[#] wg setconf wg0 /dev/fd/63
[#] ip -4 address add 10.0.0.1/24 dev wg0
[#] ip link set mtu 1420 up dev wg0
[#] iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
```

On vérifie que tout fonctionne avec les deux commandes suivantes:

`sudo wg show wg0`

Output:

```bash
interface: wg0
  public key:  Lv+jzTgyGwx74maDX83oGOOKg0+vKsvFwGptPI5aLH8=
  private key: (hidden)
  listening port: 51820
```

`ip a show wg0`

Vous devriez avoir un resultat semblable:

```bash
4: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 10.0.0.1/24 scope global wg0
       valid_lft forever preferred_lft forever
```

On fait en sorte que wireguard se lance dès le lancement de la machine:

`sudo systemctl enable wg-quick@wg0`

Configuration du pare-feu:

On ouvre le fichier `sysctl.conf` pour enlever le `#` devant la ligne `net.ipv4.ip_forward=1`:

`sudo nano /etc /sysctl.conf`

On sauvegarde la modification avec la commande:

`sudo sysctl -p`

Ce qui nous donne:

`net.ipv4.tcp_syncookies = 1`

On ouvre le port `51820` que l’on a défini comme port d’écoute dans le fichier `wg0.conf`:

`sudo ufw allow 51820/udp`

Configuration du client:

Sur le client:

On génère les clés public et privée pour le client:

`wg genkey | sudo tee /etc/wireguard/privatekey | wg pubkey | sudo tee /etc/wireguard/publickey`

On récupère les deux clés:

```bash
sudo cat /etc /wireguard /privatekey
sudo cat /etc /wireguard /publickey
```

Output:

```bash
Pv : oNeN5z9eMNjC8RqJaNV/3z6eopigwo/UtDM6kmB68mo=
Pb : xA3T+xOqOuthmb4V2l+SsL8Y8g9iHP7tWbGCZ8B4Zg4=
```
On crée le fichier de conf pour le client avec la commande :
sudo nano /etc/wireguard/wg0.conf

on ajoute le texte suivant dans le fichier wg0:
```bash
[Interface]
PrivateKey = CLIENT_PRIVATE_KEY
Address = 10.0.0.2/24

[Peer]
PublicKey = SERVER_PUBLIC_KEY
Endpoint = SERVER_IP_ADDRESS:51820
AllowedIPs = 0.0.0.0/0
```
Endpoint = SERVER_IP_ADDRESS (On entend ici l’ip du serveur écrite plus haut dans cette documentation. Celle commençant par 192 dans notre cas).

Sur le serveur:

On éxecute la commande suivante:

`sudo wg set wg0 peer CLIENT_PUBLIC_KEY allowed-ips 10.0.0.2`

Fin de configuration et lancement:
Sur le client:
On éxecute la commande qui lance la configuration du client:

`sudo wg-quick up wg0`

Enfin on éxecute la commande suivante pour vérifier si la configuration marche:

```bash
sudo wg

output :
interface: wg0
  public key: xA3T+xOqOuthmb4V2l+SsL8Y8g9iHP7tWbGCZ8B4Zg4=
  private key: (hidden)
  listening port: 53527
  fwmark: 0xca6c

peer: Lv+jzTgyGwx74maDX83oGOOKg0+vKsvFwGptPI5aLH8=
  endpoint: XXX.XXX.XXX.XXX:51820
  allowed ips: 0.0.0.0/0
  latest handshake: 53 seconds ago
  transfer: 3.23 KiB received, 3.50 KiB sent
```
la commande `sudo wg-quick down` permet de mettre fin à la configuration et de couper la connexion au VPN.
